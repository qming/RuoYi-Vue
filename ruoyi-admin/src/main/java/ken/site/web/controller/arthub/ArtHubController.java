package ken.site.web.controller.arthub;

import ken.site.ai.arthub.domain.AiArthub;
import ken.site.ai.arthub.service.AiArthubService;
import ken.site.common.core.controller.BaseController;
import ken.site.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 参数配置 信息操作处理
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/ai/arthub")
public class ArtHubController extends BaseController
{
    @Autowired
    private AiArthubService aiArthubService;

    /**
     * 获取参数配置列表
     */
    @GetMapping("/list")
    public TableDataInfo list(AiArthub aiArthub)
    {
        startPage();
        List<AiArthub> list = aiArthubService.selectList(aiArthub);
        return getDataTable(list);
    }
}
