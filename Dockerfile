FROM uhub.service.ucloud.cn/tanlian/java1.8-jmx-arthas:v1.0.1
#RUN mkdir /data/
WORKDIR /data

COPY personalaccount-service/target/*.jar /data/personalaccount-service.jar

ENTRYPOINT ["sh","-c","java -Duser.timezone=GMT+08  ${JVM_OPTIONS} -jar /data/personalaccount-service.jar"]
